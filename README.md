# How to use: 2019_07_21_UnityPackageFacilitator   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.unitypackagefacilitator":"https://gitlab.com/eloistree/2019_07_21_UnityPackageFacilitator.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.unitypackagefacilitator",                              
  "displayName": "UnityPackageFacilitator",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Tools to create the structure of new unity package under 2 minutes.",                         
  "keywords": ["Script","Tool","Productivity","Git","Unity Package"],                       
  "category": "Script",                   
  "dependencies":{"be.eloiexperiments.quickgitutility": "0.0.1"}     
  }                                                                                
```    